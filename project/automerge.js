// You need to have Java 6 installed https://www.java.com/en/download

//Includes
var fs = require('fs');
var merge = require('easy-pdf-merge');

//Application level variables
var watcher = {};
var PDFs = [];

function init(){
  watchFolder();
}

function watchFolder(){
  watcher = fs.watch('./',{encoding:'buffer'},(eventType, filename) =>{
    if( eventType == 'change' && filename.indexOf('.pdf') > 0 ){
      getFilesAndMerge( filename.toString() );
    }
  });
}

function getFilesAndMerge( thisFile ){
  //stop watching
  watcher.close();
  //merge: start
  PDFs.length = 0;
  //get front documents and add them
  frontPDFs = remapPDFs('./front', fs.readdirSync('./front'));
  PDFs = PDFs.concat(frontPDFs);
  //get the raw report and add it
  PDFs.push('./' + thisFile);
  //get back documents and add them
  backPDFs = remapPDFs('./back', fs.readdirSync('./back'));
  PDFs = PDFs.concat(backPDFs);
  //merge into a temp file
  var tempName = './' + thisFile + "._tmp.pdf";
  merge(PDFs,tempName,function(err){
    if(err){
      //post-merge-attempt present errors
      console.log(err);
    }else{
      console.log('Successfully merged ' + thisFile + '!');
      //move the raw report to the temp folder
      var UTCD = new Date().getTime();
      fs.renameSync('./' + thisFile, './temp/' + UTCD + '_' + thisFile);
      //unlink and delete the raw report- unlinking can take a few seconds and then it deletes 
      fs.unlinkSync('./temp/' + UTCD + '_' + thisFile);
      //rename the merged report back to the raw report name
      fs.renameSync(tempName, './' + thisFile);
      //duplicate the merged report
      fs.createReadStream(thisFile).pipe(fs.createWriteStream('-' + thisFile));
      //archive duplicated merged report
      archiveReport('-' + thisFile);
      //start watching again
      watchFolder();
    }
  });
  //merge: end
}

function archiveReport( thisFile ){
  var today = new Date();
  //the archive date takes the format: YYYYMMDDHHmmss-<rawhfilename>.pdf
  var archiveName = (today.getFullYear().toString()   + 
                         padLeadingZero( today.getMonth() + 1 )   +
                         padLeadingZero( today.getDate() )      + 
                         padLeadingZero( today.getHours() )     +
                         padLeadingZero( today.getMinutes() + 1 ) +
                         padLeadingZero( today.getSeconds() + 1 )).toString() +
                                         thisFile;
  fs.renameSync('./' + thisFile, './archive/' + archiveName);
  console.log('Copy of ' + thisFile.substring(1) + ' archived as ' + archiveName + '!');
}

function remapPDFs( thisPrefix, thesePDFs ){
  //fully qualify the path of the PDF file group at the array item level
  thesePDFs = thesePDFs.map( function(item){
    return thisPrefix + '/' + item;
  });
  return thesePDFs;
}

function padLeadingZero( thisNumber ){
  //pad leading zeros on numbers that are not at least 2 characters in length
  return thisNumber.toString().length < 2 ? '0' + thisNumber : thisNumber;
}

//This is where it all starts
init();