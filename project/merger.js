// You need to have Java 6 installed https://www.java.com/en/download

//Includes
var fs = require('fs');
var merge = require('easy-pdf-merge');

//Application variables


//Application methods
function init(){
  if( process.argv.length == 4 ){
    var thisFolder = '', thisMergedFile = '';
    switch(process.argv[2].indexOf('.pdf') > 0){
      case true :
        thisFolder = process.argv[3];
        thisMergedFile = process.argv[2];
        break;
      default:
        thisFolder = process.argv[2];
        thisMergedFile = process.argv[3];
    }
    makeFile( thisFolder, thisMergedFile );
  }
}

function makeFile( pdfFolder, pdfTarget ){
  var pdfFiles = remapPDFs(pdfFolder, fs.readdirSync(pdfFolder));
  merge(pdfFiles,pdfTarget,function(err){
    if(err){
      //post-merge-attempt present errors
      console.log(err);
    }else{
      console.log('Successfully created: ' + pdfTarget + '!');
    }
  });
}

//Application utilities
function remapPDFs( thisPrefix, thesePDFs ){
  //fully qualify the path of the PDF file group at the array item level
  thesePDFs = thesePDFs.map( function(item){
    return thisPrefix + '/' + item;
  });
  return thesePDFs;
}


//start the app
init();